import 'package:flutter/material.dart';
import 'package:google_map/pages/login_signup.dart';
import 'package:google_map/pages/splash_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginSignUp(),
      // home: const SplashPage(),
    );
  }
}
